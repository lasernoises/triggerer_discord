extern crate triggerer_backend;
extern crate discord;

use triggerer_backend::*;
use discord::{Discord, Connection};

pub struct DiscordBackend {
    pub discord: Discord,
    pub connection: Connection
}

impl DiscordBackend {
    pub fn new(discord: Discord) -> discord::Result<Self> {
        let connection = discord.connect()?.0;
        Ok(Self {
            discord,
            connection
        })
    }
}

impl Backend for DiscordBackend {
    type Id = u64;

    fn next_event(&mut self) -> Option<Event<Self::Id>> {
        loop {
            match self.connection.recv_event() {
                Ok(discord::model::Event::MessageCreate(msg)) => {
                    return Some(Event::Message(Message {
                        chat: msg.channel_id.0,
                        author: msg.author.id.0,
                        content: msg.content
                    }));
                }
                Ok(_) => (),
                Err(_) => return None
            }
        }
    }

    fn send_message(&mut self, chat: Self::Id, content: &str) {
        self.discord.send_message(discord::model::ChannelId(chat), content, "", false);
    }

    fn user_name(&self, id: Self::Id) -> Option<String> {
        //        match self.discord.get_channel(discord::model::ChannelId(id)) {
        //            Ok(channel) => channel.
        //        }
        unimplemented!()
    }

    fn chat_name(&self, id: Self::Id) -> Option<String> {
        unimplemented!()
    }

    fn users(&self, chat: Self::Id) -> Vec<Self::Id> {
        unimplemented!()
    }

    fn chats(&self) -> Vec<Self::Id> {
        unimplemented!()
    }
}
